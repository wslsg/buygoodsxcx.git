// components/searchlist/searchlist.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    data:{
      value:[],
      type:Array
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    sou(e){
      this.triggerEvent('sou',e.currentTarget.dataset.id)
    }
  }
})