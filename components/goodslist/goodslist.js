// components/goodslist/goodslist.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    goodsList:{
      value:[],
      type:Array
    },
    fl:{
      value:[],
      type:Array
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    option1: [
      { text: '价格由高到底', value: 'desc' },
      { text: '价格由低到高', value: 'asc' },
    ],
    value1: 'desc',
  },

  /**
   * 组件的方法列表
   */
  methods: {
    changeprice(a){
      this.triggerEvent('changeprice',a.detail)
    },
    changetype(a){
      this.triggerEvent('changetype',a.detail)
    }
  }
})