// components/hishot/hishot.js
import {dellsapi} from "../../request/api"
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    historyKeywordList:{
      type:Array,
      value:[]
    }
    ,
    hotKeywordList:{
      type:Object,
      value:{}
    }
  },
  
  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    sou(e){
      this.triggerEvent('sou',e.currentTarget.dataset.id)
    },
  async  del(){
    let res=await dellsapi()
    this.triggerEvent('del',res.errno)
    }
  }
})