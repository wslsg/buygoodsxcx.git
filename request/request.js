const baseUrl="http://tech.wolfcode.cn:8001"
export default function request(url,params={}){
  return new Promise((resolve,reject)=>{
    wx.showLoading({
      title: '加载中...',
    })
    let header=params.header||{}
    let token=wx.getStorageSync('token')
    if(token){
      header['X-Nideshop-Token']=token
    }
    wx.request({
      url: baseUrl+url,
      data:params.data||{},
      header:header,
      timeout:8000,
      method:params.method||'GET',
      success:(res)=>{
        wx.hideLoading()
        resolve(res.data)
      },
      fail:(err)=>{
        wx.hideLoading()
        reject(err)
      }
    })
  })
}