// pages/type/type.js
import {typeapi,typegoodsapi } from '../../request/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    activeKey:0,
    categoryList:[],
    currentCategory:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
async  onLoad(options) {
    let res=await typeapi()
    console.log(res);
    let {categoryList,currentCategory}=res.data
    this.setData({
      categoryList,currentCategory
    })
  },
 async onChange(event) {
    let iid=this.data.categoryList[event.detail].id
    let a=await typegoodsapi({
      data:{
        id:String(iid)
      }
    })
    this.setData({
      currentCategory:a.data.currentCategory
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getTabBar().setData({
      active:2
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})