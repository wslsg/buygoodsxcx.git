// pages/search/search.js
import {hishotapi,searchlistapi,goodslistapi} from '../../request/api'
let se
Page({

  /**
   * 页面的初始数据
   */
  data: {
    historyKeywordList:[],
    hotKeywordList:[],
    show:1,
    data:[],
    goodsList:[],
    filterCategory:[],
    fl:[],
    sort:'',
    order:'',
    val:'',
    categoryId:'',
    page:1
  },
  onCancel(){
    wx.navigateBack()
  },
onSearch(b){
    this.setData({
      show:3,
      val:b.detail
    })
    this.getgoods()
  },
 async getgoods(){
  let res=await goodslistapi({
    data:{
      keyword:this.data.val,
      page:this.data.page,
      size:10,
      order:this.data.order,
      sort:this.data.sort,
      categoryId:this.data.categoryId
    }
  })
  let {goodsList,filterCategory}=res.data
  console.log(res);
  this.setData({
    goodsList,
    filterCategory
  })
  let fll=this.data.filterCategory.map(val=>{
    return {text:val.name,value:val.id}
  })
  this.setData({
    fl:fll
  })
  },
  changeprice(a){
    this.setData({
      order:a.detail,
      sort:'price'
    })
    this.getgoods()
  },
  changetype(b){
    console.log(b.detail);
    this.setData({
      categoryId:b.detail,
      sort:'id'
    })
    this.getgoods()
  },
change(a){
    this.setData({
      show:2,
      val:a.detail
    })
    clearTimeout(se)
    se=setTimeout(async() => {
      let res=await searchlistapi({
        data:{
          keyword:this.data.val
        }
      })
      this.setData({
        data:res.data
      })
    }, 500);
    
  },
  /**
   * 生命周期函数--监听页面加载
   */
async  onLoad(options) {
  let res=await hishotapi()
  console.log(res);
  let {historyKeywordList,hotKeywordList}=res.data
  this.setData({
    historyKeywordList,hotKeywordList
  })
  },
  sou(a){
    console.log(a.detail);
    this.setData({
      val:a.detail,
      show:3
    })
    this.getgoods()
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },
  del(a){
    console.log(a.detail);
    if(a.detail==0){
      this.onLoad()
      wx.showLoading({
        title: '删除成功',
      })
    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
 onReachBottom() {
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})