// pages/topic/topic.js
import {topicapi} from "../../request/api"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    data:[],
    page:1,
    totalpages:'',
    pred:false,
    nexd:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
 onLoad(options) {
  this.topic()
  },
  pres(){
    if(this.data.page==1){
      this.setData({
        page:1,
        pred:true
      })
      return false
    }else{
      wx.pageScrollTo({
        scrollTop:0
      })
      this.setData({
        page:this.data.page-1,
        nexd:false
      })
      this.topic()
    }
  },
  next(){
    if(this.data.page==this.data.totalpages){
      this.setData({
        nexd:true
      })
      return false
    }else{
      wx.pageScrollTo({
        scrollTop:0
      })
      this.setData({
        page:this.data.page+1,
        pred:false,
      })
      this.topic()
    }
  },
 async topic(){
    let res=await topicapi({
      data:{
        page:this.data.page,
        size:10
      }
    })
    console.log(res);
    this.setData({
      data:res.data.data,
      totalpages:res.data.totalPages
    })
    if(this.data.page==1){
      this.setData({
        pred:true,
      })
    }
    if(this.data.page==this.data.totalpages){
      this.setData({
        nexd:true
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getTabBar().setData({
      active:1
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})