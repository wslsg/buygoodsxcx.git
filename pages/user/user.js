let a
let b
import { loginapi } from "../../request/api";
import Dialog from '@vant/weapp/dialog/dialog';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isshow:true,
    show: false,
    usename:'',
    pwd:'',
    name:''
  },
  /**
   * 生命周期函数--监听页面显示
   */
  showPopup() {
    this.setData({ show: true });
  },
  onLoad(){
    let token=wx.getStorageSync('token')
    let name=wx.getStorageSync('name')
    if(token){
      this.setData({
        isshow:false,
        name
      })
    }
  },
  tuilogin(){
    Dialog.confirm({
      title: '退出账号确认',
      message: '您是否想要退出当前账号',
    })
      .then(() => {
        wx.removeStorageSync('token')
        wx.removeStorageSync('name')
        this.setData({
          isshow:true
        })
        wx.showToast({
          title: '退出成功',
        })
      }).catch(() => {
        return false
      });
  },
  onClose() {
    this.setData({ show: false });
  },
 async login(){
    if(!this.data.pwd||!this.data.usename){
      return false
    }else{
      let res=await loginapi({
        data:{
          username:this.data.usename,
          pwd:this.data.pwd
        },
        method:'Post'
      })
      if(res.errno==0){
        wx.setStorageSync('token', res.data.token)
        wx.setStorageSync('name', res.data.userInfo.username)
        let name=wx.getStorageSync('name')
        this.setData({
          name
        })
        if(wx.getStorageSync('token')){
          this.setData({
            show:false,
            isshow:false
          })
        }
      }
    }
  },
  getuse(e){
    clearTimeout(a)
    a=setTimeout(() => {
      this.setData({
        usename:e.detail.value
      })
    }, 500);
  },
  getpwd(e){
    clearTimeout(b)
    b=setTimeout(() => {
      this.setData({
        pwd:e.detail.value
      })
    }, 500);
  },
  onShow() {
    this.getTabBar().setData({
      active: 4
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})