// pages/tabgoods/tabgoods.js
import {homeapi, hometypeapi,hometypegoodsapi} from "../../request/api"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    active:0,
    brotherCategory:[],
    currentCategory:{},
    categoryId:'1005000',
    goodsList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
 async onLoad(options) {
    let id=options.id
    let res=await hometypeapi({data:{id}})
    console.log(res);
    let {brotherCategory,currentCategory}=res.data
    this.setData({
      brotherCategory,
      currentCategory
    })
    this.getgoodslist()
  },
 async getgoodslist(){
    let ress=await hometypegoodsapi({data:{categoryId:this.data.categoryId,page:1,size:1000}})
    console.log(ress);
    this.setData({
      goodsList:ress.data.goodsList
    })
  },
changegoods(a){
    console.log(a.detail.index);
    this.setData({categoryId:this.data.brotherCategory[a.detail.index].id,active:a.detail.index})
  this.getgoodslist()
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})