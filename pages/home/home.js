// pages/home/home.js
import {homeapi} from "../../request/api"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    banner:[],
    brandList:[],
    channel:[],
    hotGoodsList:[],
    newGoodsList:[],
    topicList:[],
    categoryList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
 async onLoad(options) {
    let res=await homeapi()
    let {banner,brandList,channel,hotGoodsList,newGoodsList,topicList,categoryList}=res.data
    this.setData(
      {banner,brandList,channel,hotGoodsList,newGoodsList,topicList,categoryList}
      )
  },
  gosearch(){
    wx.navigateTo({
      url: '/pages/search/search',
    })
  },
  gotabgood(e){
    wx.navigateTo({
      url: '/pages/tabgoods/tabgoods?id='+e.currentTarget.dataset.id,
    })
  },
  gozzs(e){
    wx.navigateTo({
      url: '/pages/zzs/zzs?id='+e.currentTarget.dataset.id,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getTabBar().setData({
      active:0
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})