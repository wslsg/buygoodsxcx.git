// custom-tab-bar/index.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    active:0,
    tabpath:[
      '/pages/home/home',
      '/pages/topic/topic',
      '/pages/type/type',
      '/pages/cart/cart',
      '/pages/user/user',

    ]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onChange(a){
      this.setData({
        active:a.detail
      })
      wx.switchTab({
        url: this.data.tabpath[this.data.active],
      })
    }
  }
})